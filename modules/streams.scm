(define true #t)
(define false #f)

(define the-empty-stream '())
(define (stream-null? stream) (eq? stream '()))
(define (make-stream) the-empty-stream)

(define (force delayed-object) (delayed-object))

(define (memo-proc proc)
  (let ((already-run? false)
	(result false))
    (lambda ()
      (if (not already-run?)
	  (begin (set! result (proc))
		 (set! already-run? true)
		 result)
	  result))))

(define-syntax delay
  (syntax-rules ()
    ((delay exp)
     (memo-proc (lambda () exp)))))

(define-syntax cons-stream
  (syntax-rules ()
    ((cons-stream a b)
     (cons a (delay b)))))

(define (stream-car stream) (car stream))
(define (stream-cdr stream) (force (cdr stream)))

(define (stream-ref stream n)
  (if (= 0 n)
      (stream-car stream)
      (stream-ref (stream-cdr stream) (- n 1))))

(define (integers-starting-from n)
  (cons-stream n (integers-starting-from (+ 1 n))))

(define int (integers-starting-from 1))

(define (stream-enumerate n m)
  (if (> n m)
      '()
      (cons-stream n (stream-enumerate (+ 1 n) m))))

(define (stream-map proc . argstreams)
  (if (stream-null? (car argstreams))
      the-empty-stream
      (cons-stream
       (apply proc (map stream-car argstreams))
       (apply stream-map
	      (cons proc (map stream-cdr argstreams))))))

(define (mul-streams s1 s2)
  (stream-map * s1 s2))

(define factorials
  (cons-stream 1 (mul-streams factorials (integers-starting-from 2))))

(define (add-streams s1 s2)
  (stream-map + s1 s2))

(define (partial-sum s)
  (cons-stream (stream-car s)
	       (add-streams (partial-sum s) (stream-cdr s))))

(define (merge s1 s2)
  (cond ((stream-null? s1) s2)
	((stream-null? s2) s1)
	(else
	 (let ((s1car (stream-car s1))
	       (s2car (stream-car s2)))
	   (cond ((< s1car s2car)
		  (cons-stream s1car
			       (merge (stream-cdr s1) s2)))
		 ((> s1car s2car)
		  (cons-stream s2car
			       (merge s1 (stream-cdr s2))))
		 (else
		  (cons-stream s1car
			       (merge (stream-cdr s1)
				      (stream-cdr s2)))))))))

(define (scale-stream stream factor)
  (stream-map (lambda (x) (* x factor)) stream))

(define S (cons-stream 1 (merge
			  (merge (scale-stream S 2)
				 (scale-stream S 3))
			  (scale-stream S 5))))

(define (take n stream)
  (if (= n 0)
      '()
      (cons (stream-car stream) (take (- n 1) (stream-cdr stream)))))

(define reciprocals
  (stream-map (lambda (n) (/ 1 n)) (integers-starting-from 1)))

(define (integrate-series series)
  (mul-streams reciprocals series))

(define exp-series
  (cons-stream 1 (integrate-series exp-series)))

(define cosine-series
  (cons-stream 1 (integrate-series sine-series)))

(define sine-series
  (cons-stream 0 (scale-stream (integrate-series cosine-series) -1)))

(define (mul-series s1 s2)
  (cons-stream (* (stream-car s1)
		  (stream-car s2))
	       (add-streams (scale-stream (stream-cdr s1)
					  (stream-car s2))
			    (mul-series s1
					(stream-cdr s2)))))

;; (add-streams (mul-series cosine-series cosine-series) (mul-series sine-series sine-series))

(define (invert-unit-series u)
  (cons-stream 1
	       (scale-stream
		(mul-series (stream-cdr u)
			    (invert-unit-series u))
		-1)))

(define (invert-series s)
  (if (= 0 (stream-car s))
      (error "Series not invertible")
      (let ((u (/ 1 (stream-car s))))
	(cons-stream u
		     (scale-stream
		      (mul-series (stream-cdr s)
				  (invert-series s))
		      u)))))

(define (div-series s1 s2)
  (mul-series s1
	      (invert-series s2)))

(define (partial-sums s)
  (cons-stream (stream-car s)
	       (add-streams (partial-sums s) (stream-cdr s))))

(define (sqrt-improve guess x)
  (average guess (/ x guess)))

(define (average x y)
  (/ (+ x y)
     2))

(define (sqrt-stream x)
  (define guesses
    (cons-stream 1.0
		 (stream-map (lambda (guess) (sqrt-improve guess x))
			     guesses)))
  guesses)

(define (pi-summands n)
  (cons-stream (/ 1.0 n)
	       (stream-map - (pi-summands (+ n 2)))))

(define pi-stream
  (scale-stream (partial-sums (pi-summands 1)) 4))

(define (euler-transform s)
  (let ((s0 (stream-ref s 0))
	(s1 (stream-ref s 1))
	(s2 (stream-ref s 2)))
    (cons-stream (- s2 (/ (square (- s2 s1))
			  (+ s0 (* -2 s1) s2)))
		 (euler-transform (stream-cdr s)))))

(define (interleave s1 s2)
  (if (stream-null? s1)
      s2
      (cons-stream (stream-car s1)
		   (interleave s2 (stream-cdr s1)))))

(define (pairs s t)
  (cons-stream
   (list (stream-car s) (stream-car t))
   (interleave
    (stream-map (lambda (x) (list (stream-car s) x))
		(stream-cdr t))
    (pairs (stream-cdr s) (stream-cdr t)))))

(define (stream-filter pred? stream)
  (cond ((stream-null? stream) the-empty-stream)
	((pred? (stream-car stream))
	 (cons-stream (stream-car stream)
		      (stream-filter pred? (stream-cdr stream))))
	(else (stream-filter pred? (stream-cdr stream)))))

(define (all-pairs s1 s2)
  (interleave (pairs s1 s2)
	      (stream-map swap-pair
			  (stream-filter (lambda (pair)
					   (not (= (car pair) (cadr pair))))
					 (pairs s2 s1)))))

(define (swap-pair pair)
  (list (cadr pair) (car pair)))

(define (triples s t u)
  (cons-stream
   (list
    (stream-car s)
    (stream-car t)
    (stream-car u))
   (interleave
    (stream-map
     (lambda (x) (append (list (stream-car s)) x))
     (stream-cdr (pairs t u)))
    (triples
     (stream-cdr s)
     (stream-cdr t)
     (stream-cdr u)))))

(define (weighted-merge w s1 s2)
  (cond ((stream-null? s1) s2)
	((stream-null? s2) s1)
	(else
	 (let ((s1car (stream-car s1))
	       (s2car (stream-car s2)))
	   (if (<= (w s1car) (w s2car))
		(cons-stream s1car
			     (weighted-merge w (stream-cdr s1) s2))
		(cons-stream s2car
			     (weighted-merge w s1 (stream-cdr s2))))))))

(define (weighted-pairs w s t)
  (cons-stream
   (list (stream-car s) (stream-car t))
   (weighted-merge
    w
    (stream-map (lambda (x) (list (stream-car s) x))
		(stream-cdr t))
    (weighted-pairs w (stream-cdr s) (stream-cdr t)))))

(define ordered-pairs
  (weighted-pairs
   (lambda (pair) (apply + pair))
   int
   int))

(define (divisible? x by)
  (= 0 (remainder x by)))

(define strange-pairs
  (weighted-pairs
   (lambda (pair)
     (let ((fst (car pair))
	   (snd (cadr pair)))
       (+ (* 2 fst)
	  (* 3 snd)
	  (* 5 fst snd))))
   (stream-filter (lambda (x)
		    (not (or (divisible? x 3)
			     (divisible? x 2)
			     (divisible? x 5))))
		  int)
   (stream-filter (lambda (x)
		    (not (or (divisible? x 3)
			     (divisible? x 2)
			     (divisible? x 5))))
		  int)))

(define (cube x) (* x x x))

(define (sum-of-cubes pair)
  (let ((x (car pair))
	(y (cadr pair)))
    (+ (cube x) (cube y))))

(define sum-of-cubes-stream
  (weighted-pairs
   (lambda (pair) (sum-of-cubes pair))
   int
   int))

(define (check-consecutives weight stream)
  (let ((fst (stream-car stream))
	(snd (stream-car (stream-cdr stream))))
    (if (= (weight fst) (weight snd))
	(cons-stream (list fst snd)
		     (check-consecutives weight (stream-cdr stream)))
	(check-consecutives weight (stream-cdr stream)))))

(define ramanujan-numbers
  (check-consecutives
   sum-of-cubes
   sum-of-cubes-stream))

(define ramanujan-stream
  (stream-map
   (lambda (pair)
     (cons pair (sum-of-cubes (car pair))))
   ramanujan-numbers))

(define (sum-of-squares pair)
  (+ (square (car pair)) (square (cadr pair))))

(define sum-of-squares-stream
  (weighted-pairs sum-of-squares int int))

(define (check-3-consecutives weight stream)
  (let ((fst (stream-car stream))
	(snd (stream-car (stream-cdr stream)))
	(trd (stream-car (stream-cdr (stream-cdr stream)))))
    (let ((three (list fst snd trd)))
      (if (apply = (map weight three))
	  (cons-stream three
		       (check-3-consecutives weight (stream-cdr stream)))
	  (check-3-consecutives weight (stream-cdr stream))))))

(define ex-372-numbers
  (check-3-consecutives
   sum-of-squares
   sum-of-squares-stream))

(define ex-372-stream
  (stream-map
   (lambda (pair)
     (cons pair (sum-of-squares (car pair))))
   ex-372-numbers))

(define (integral integrand initial-value dt)
  (define int
    (cons-stream initial-value
		 (add-streams (scale-stream integrand dt)
			      int)))
  int)

(define (solve f y0 dt)
  (define y (integral dy y0 dt))
  (define dy (stream-map f y))
  y)

(define (delayed-integral delayed-integrand initial-value dt)
  (define int
    (cons-stream initial-value
		 (let ((integrand (force delayed-integrand)))
		   (add-streams (scale-stream integrand dt)
				int))))
  int)

(define (solve f y0 dt)
  (define y (delayed-integral-2 (delay dy) y0 dt))
  (define dy (stream-map f y))
  y)

(define (integral-2 integrand initial-value dt)
  (cons-stream
   initial-value
   (if (stream-null? integrand)
       the-empty-stream
       (integral (stream-cdr integrand)
		 (+ (* dt (stream-car integrand))
		    initial-value)
		 dt))))

(define (create-base-signal from dt)
  (cons-stream from
	       (create-base-signal (+ from dt) dt)))

(define (create-function-signal f from dt)
  (stream-map f (create-base-signal from dt)))

(define square-signal
  (create-function-signal square 0 0.001))

(define int-square-signal
  (create-function-signal
   (lambda (x) (* (/ 1 3) (cube x)))
   0
   0.001))

(define (delayed-integral-2 delayed-integrand initial-value dt)
  (cons-stream
   initial-value
     (if (stream-null? delayed-integrand)
	 the-empty-stream
	 (let ((integrand (force delayed-integrand)))
	   (integral (stream-cdr integrand)
		     (+ (* dt (stream-car integrand))
			initial-value)
		     dt)))))

(define e-stream
  (solve (lambda (x) x)
	 1
	 0.001))

(define e-approx (stream-ref e-stream 100))

(define (RLC R L C dt)
  (lambda (vC0 iL0)
    (define vC (delayed-integral (delay dvC) vC0 dt))
    (define iL (delayed-integral (delay diL) iL0 dt))
    (define dvC (scale-stream iL (- (/ 1 C))))
    (define diL (add-streams (scale-stream vC (/ 1 L))
			     (scale-stream iL (- (/ R L)))))
    (stream-map cons vC iL)))

(define my-RLC-circuit (RLC 1 0.2 1 0.1))

(define my-RLC-circuit-run (my-RLC-circuit 0 10))

(import (srfi 27))

(define (random-int-in-range low high)
  (+ low (* (random-real) high)))

(define (random-stream-in-range low high)
  (cons-stream (random-int-in-range low high)
	       (random-stream-in-range low high)))

(define (experiment-stream f x0 x1 y0 y1)
  (stream-map (lambda (x y) (<= y (f x)))
	      (random-stream-in-range x0 x1)
	      (random-stream-in-range y0 y1)))

(define (stream-estimate-integral f x0 x1 y0 y1)
  (let ((area (* (- x1 x0)
		 (- y1 y0))))
    (scale-stream (monte-carlo (experiment-stream f x0 x1 y0 y1) 0 0) area)))

(define (monte-carlo experiment-stream passed failed)
  (define (next passed failed)
    (cons-stream
     (/ passed (+ passed failed))
     (monte-carlo
      (stream-cdr experiment-stream) passed failed)))
  (if (stream-car experiment-stream)
      (next (+ passed 1) failed)
      (next passed (+ failed 1))))
