(define (add x y) (apply-generic 'add x y))
(define (sub x y) (apply-generic 'sub x y))
(define (mul x y) (apply-generic 'mul x y))
(define (div x y) (apply-generic 'div x y))
(define (min x) (apply-generic 'min x))

(define (equ? x y) (apply-generic 'equ? x y))
(define (=zero? x) (apply-generic '=zero? x))

(define (attach-tag type-tag contents) (cons type-tag contents))
(define (type-tag number)
  (if (pair? number)
      (car number)
      'scheme-number))

(define (contents number)
  (if (pair? number)
      (cdr number)
      number))

(load "tables.scm")

(define optable (make-table-proc))

(define put
  (lambda (k1 k2 v)
    ((optable 'insert-proc!) k1 k2 v)))

(define get
  (lambda (k1 k2)
    ((optable 'lookup-proc) k1 k2)))

(define (apply-generic-naive op . args)
  (let ((type-tags (map type-tag args)))
    (let ((proc (get op type-tags)))
      (if proc
	  (apply proc (map contents args))
	  (error "No method for these types: APPLY-GENERIC"
		 (list op type-tags))))))

(define (install-scheme-number-package)

  (put 'add '(scheme-number scheme-number)
       (lambda (x y) (+ x y)))
  (put 'sub '(scheme-number scheme-number)
       (lambda (x y) (- x y)))
  (put 'mul '(scheme-number scheme-number)
       (lambda (x y) (* x y)))
  (put 'div '(scheme-number scheme-number)
       (lambda (x y) (/ x y)))
  (put 'min 'scheme-number
       (lambda (x) (- x)))
  (put 'make 'scheme-number (lambda (x) x))

  (put 'equ? '(scheme-number scheme-number) (lambda (x y) (= x y)))
  (put '=zero? '(scheme-number) (lambda (x) (= 0 x)))

  'done)

(define (make-scheme-number n)
  ((get 'make 'scheme-number) n))

(define (install-rational-package)
  ;; internal procedures
  (define (numer x) (car x))
  (define (denom x) (cdr x))
  (define (make-rat n d)
    (let ((g (gcd n d)))
      (cons (/ n g) (/ d g))))

  (define (add-rat x y)
    (make-rat (+ (* (numer x) (denom y))
		 (* (numer y) (denom x)))
	      (* (denom x) (denom y))))

  (define (sub-rat x y)
    (make-rat (- (* (numer x) (denom y))
		 (* (numer y) (denom x)))
	      (* (denom x) (denom y))))

  (define (mul-rat x y)
    (make-rat (* (numer x) (numer y))
	      (* (denom x) (denom y))))

  (define (div-rat x y)
    (make-rat (* (numer x) (denom y))
	      (* (denom x) (numer y))))

  (define (min-rat x)
    (make-rat (- (numer x)) (denom x)))

  (define (equ-rational? x y)
    (= (* (numer x) (denom y))
       (* (denom x) (numer y))))

  (define (=zero-rational? x) (= (numer x) 0))

  ;;interface
  (define (tag x) (attach-tag 'rational x))

  (put 'add '(rational rational)
       (lambda (x y) (tag (add-rat x y))))
  (put 'sub '(rational rational)
       (lambda (x y) (tag (sub-rat x y))))
  (put 'mul '(rational rational)
       (lambda (x y) (tag (mul-rat x y))))
  (put 'div '(rational rational)
       (lambda (x y) (tag (div-rat x y))))
  (put 'min 'rational
       (lambda (x) (tag (min-rat x))))
  (put 'make 'rational
       (lambda (n d) (tag (make-rat n d))))

  (put 'equ? '(rational rational) equ-rational?)
  (put '=zero? 'rational =zero-rational?)

  'done)

(define (make-rational n d)
  ((get 'make 'rational) n d))

(define (install-rectangular-package)
  ;; internal procedures
  (define (real-part z) (car (contents z)))
  (define (imag-part z) (cdr (contents z)))
  (define (make-from-real-imag x y) (cons x y))
  (define (magnitude z)
    (sqrt (+ (square (real-part z))
	     (square (imag-part z)))))
  (define (angle z)
    (atan (imag-part z) (real-part z)))
  (define (make-from-mag-ang r a)
    (cons (* r (cos a)) (* r (sin a))))

  ;; interface to the rest of the system
  (define (tag x) (attach-tag 'rectangular x))
  (put 'real-part 'rectangular real-part)
  (put 'imag-part 'rectangular imag-part)
  (put 'angle 'rectangular angle)
  (put 'magnitude 'rectangular magnitude)
  (put 'make-from-real-imag 'rectangular
       (lambda (x y) (tag (make-from-real-imag x y))))
  (put 'make-from-mag-ang 'rectangular
       (lambda (r a) (tag (make-from-mag-ang r a))))
  'done)

(define (install-polar-package)
  ;;internal procedures
  (define (magnitude z) (car (contents z)))
  (define (angle z) (cdr (contents z)))
  (define (make-from-mag-ang r a) (cons r a))
  (define (real-part z) (* (magnitude z) (cos (angle z))))
  (define (imag-part z) (* (magnitude z) (sin (angle z))))
  (define (make-from-real-imag x y)
    (cons (sqrt (+ (square x) (square y)))
	  (atan y x)))

  ;; interface to the rest of the system
  (define (tag x) (attach-tag 'polar x))
  (put 'real-part 'polar real-part)
  (put 'imag-part 'polar imag-part)
  (put 'magnitude 'polar magnitude)
  (put 'angle 'polar angle)
  (put 'make-from-real-imag 'polar
       (lambda (x y) (tag (make-from-real-imag x y))))
  (put 'make-from-mag-ang 'polar
       (lambda (r a) (tag (make-from-mag-ang r a))))
  'done)

(define (install-complex-package)
  ;; imported from rectangular and polar packages
  (define (make-from-real-imag x y)
    ((get 'make-from-real-imag 'rectangular) x y))
  (define (make-from-mag-ang r a)
    ((get 'make-from-mag-ang 'polar) r a))

  (define (real-part z)
    (let ((zz (contents z)))
      ((get 'real-part (list (type-tag z))) z)))

  ;; internal procedures
  (define (add-complex z1 z2)
    (make-from-real-imag (+ (real-part z1) (real-part z2))
			 (+ (imag-part z1) (imag-part z2))))

  (define (sub-complex z1 z2)
    (make-from-real-imag (- (real-part z1) (real-part z2))
			 (- (imag-part z1) (imag-part z2))))

  (define (mul-complex z1 z2)
    (make-from-mag-ang (* (magnitude z1) (magnitude z2))
		       (+ (angle z1) (angle z2))))

  (define (div-complex z1 z2)
    (make-from-mag-ang (/ (magnitude z1) (magnitude z2))
		       (- (angle z1) (angle z2))))

  (define (equ-complex? z1 z2)
    (and (= (real-part z1) (real-part z2))
	 (= (imag-part z1) (imag-part z2))))

  (define (=zero-complex? z) (= (magnitude z) 0))

  ;; interface
  (define (tag z) (attach-tag 'complex z))

  (put 'real-part 'complex
       (lambda (z) (real-part z)))
  (put 'add '(complex complex)
       (lambda (z1 z2) (tag (add-complex z1 z2))))
  (put 'sub '(complex complex)
       (lambda (z1 z2) (tag (sub-complex z1 z2))))
  (put 'mul '(complex complex)
       (lambda (z1 z2) (tag (mul-complex z1 z2))))
  (put 'div '(complex complex)
       (lambda (z1 z2) (tag (div-complex z1 z2))))
  (put 'min 'complex
       (lambda (z) (make-from-real-imag
		    (- (real-part z))
		    (- (imag-part z)))))

  (put 'real-part 'complex real-part)
  (put 'imag-part 'complex imag-part)
  (put 'magnitude 'complex magnitude)
  (put 'angle 'complex angle)

  (put 'make-from-real-imag 'complex
       (lambda (x y) (tag (make-from-real-imag x y))))
  (put 'make-from-mag-ang 'complex
       (lambda (r a) (tag (make-from-mag-ang r a))))

  (put 'equ? '(complex complex) equ-complex?)
  (put '=zero? 'complex =zero-complex?)

  'done)

(define (make-complex-from-real-imag x y)
  ((get 'make-from-real-imag 'complex) x y))

(define (make-complex-from-mag-ang r a)
  ((get 'make-from-mag-ang 'complex) r a))

(define (install-coercion-package)

  (define (rational->scheme-number x)
    (let ((r (contents x)))
      (/ (enum r) (denom r))))

  (define (scheme-number->complex x)
    (make-complex-from-real-imag (contents x) 0))

  (define (rational->complex x)
    (scheme-number->complex (rational->scheme-number (contents x))))

  (put-coercion 'rational 'scheme-number rational->scheme-number)
  (put-coercion 'scheme-number 'complex scheme-number->complex)
  (put-coercion 'rational 'complex rational->complex)
  'done)

(define (apply-generic op . args)
  ;; first we test how many arguments there are
  (cond ((= (length args) 0) (error "No arguments"))  ;; if none, undefined
	 ;; if only one, return it
	((= (length args) 1) args)
	;; if exactly two, implement the usual one
	((= (length args) 2)
	 (let ((type-tags (map type-tag args)))
	   (let ((proc (get op type-tags)))
	     (if proc
		 (apply proc (map contents args))
		 (let ((type1 (car type-tags))
		       (type2 (car type-tags))
		       (a1 (car args))
		       (a2 (cadr args)))
		   (let ((t1->t2 (get-coercion type1 type2))
			 (t2->t1 (get-coercion type2 type1)))
		     (cond (t1->t2
			    (apply-generic op (t1->t2 a1) a2))
			   (t2->t1
			    (apply-generic op a1 (t2->t1 a2)))
			   (else
			    (error "No method for these types"
				   (list op type-tags))))))))))
	;; if there are more then 2, extract the first two and the rest
	(else
	 (let ((x (car args))
	       (y (cadr args))
	       (rest (cdr (cdr args))))
	   ;; call apply-generic on the first two, and then
	   ;; on the result together with the rest
	   (apply-generic op (apply-generic op '(x y)) rest)))))

(define (simplify x)
  (let ((result ((get-simplify (type-tag x)) x)))
    (let ((success? (car result))
	  (value (cadr result)))
      (if success?
	  (simplify value)
	  x))))

(define (install-simplification-table)
  (define (complex->scheme-number x)
    (let ((z (contents x)))
      (if (= (imag-part z) 0)
	  (list #t (make-scheme-number (real-part z)))
	  (list #f '()))))

  (define (scheme-number->rational x)
    ;; This will essentially make an integer out of it.
    ;; It makes absolutely no sense, I am fully aware.
    (let ((n (contents x)))
      (if (integer? n)
	  (list #t (make-rational n 1))
	  (list #f '()))))

  (define (complex->rational x)
    (scheme-number->rational (complex->scheme-number (contents x))))

  (put-simplify 'complex 'scheme-number complex->scheme-number)
  (put-simplify 'scheme-number 'rational scheme-number->rational)
  (put-simplify 'complex 'rational complex->rational)
  'done)
