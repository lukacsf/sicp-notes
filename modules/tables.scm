(define (lookup key table)
  (let ((record (assoc key (cdr table))))
    (if record
	(cdr record)
	#f)))

(define (assoc key records)
  (cond ((null? records) #f)
	((equal? key (caar records)) (car records))
	(else (assoc key (cdr records)))))

(define (insert! key value table)
   (let ((record (assoc key (cdr table))))
     (if record
	 (set-cdr! record value)
	 (set-cdr! table
		   (cons (cons key value)
			 (cdr table))))))

(define (make-table) (list '*table*))

(define (insert-2d! key1 key2 value table)
  (let ((subtable (assoc key1 (cdr table))))
    (if subtable
	(let ((record (assoc key2 (cdr subtable))))
	  (if record
	      (set-cdr! record value)
	      (set-cdr! subtable
			(cons (cons key2 value)
			      (cdr subtable)))))
	(set-cdr! table
		  (cons (list key1 (cons key2 value))
			(cdr table)))))
  'ok)

(define (lookup-2d key1 key2 table)
  (let ((subtable (assoc key1 (cdr table))))
    (if subtable
	(let ((record (assoc key2 (cdr subtable))))
	  (if record
	      (cdr record)
	      #f))
	#f)))

(define (make-table-proc)
  (let ((local-table (list '*table*)))
    (define (lookup key1 key2)
      (let ((subtable (assoc key1 (cdr local-table))))
	(if subtable
	    (let ((record (assoc key2 (cdr subtable))))
	      (if record
		  (cdr record)
		  #f))
	    #f)))
    (define (insert! key1 key2 value)
      (let ((subtable (assoc key1 (cdr local-table))))
	(if subtable
	    (let ((record (assoc key2 (cdr subtable))))
	      (if record
		  (set-cdr! record value)
		  (set-cdr! subtable
			    (cons (cons key2 value)
				  (cdr subtable)))))
	    (set-cdr! local-table
		     (cons (list key1 (cons key2 value))
			   (cdr local-table)))))
      'ok)
    (define (dispatch m)
      (cond ((eq? m 'lookup-proc) lookup)
	    ((eq? m 'insert-proc!) insert!)
	    (else (error "Unknown operation: TABLE" m))))
    dispatch))

(define (lookup-hd table keys)
  (let ((key (car keys))
	(rest-of-keys (cdr keys)))
    (let ((subtable (assoc key (cdr table))))
      (if subtable
	  (if (pair? (cdr subtable))
	      (if (null? rest-of-keys)
		  (error "Too few keys")
		  (lookup-hd subtable rest-of-keys))
	      (if (null? rest-of-keys)
		  (cdr subtable)
		  (error "Too many keys")))
	  #f))))

(define (insert-hd! table value keys)
  (let ((key (car keys))
	(rest-of-keys (cdr keys)))
    (let ((subtable (assoc key (cdr table))))
      (if subtable
	  ;; check if there are more dimensions
	  (if (pair? (cdr subtable))
	      ;; if there are, check if there are more keys
	      (if (null? rest-of-keys)
		  ;; if there are go deeper
		  (insert-hd! subtable value rest-of-keys)
		  ;; if there are not, too bad
		  (error "Too few keys"))
	      ;; if there are not, check if there are more keys
	      (if (null? rest-of-keys)
		  ;; if there are, we have dimension mismatch
		  (error "Dimension mismatch")
		  ;; if there are not, insert
		  (set-cdr! subtable value)))
	  ;; if there is no match for the first key, create new subtable
	  (let ((new-subtable (create-new-subtable key value rest-of-keys)))
	    ;; and insert it
	    (set-cdr! table (cons new-subtable (cdr table)))))))
  'ok)

(define (create-new-subtable name value keys)
  (if (null? (cdr keys))
      (list name (cons (car keys) value))
      (list name (create-new-subtable (car keys) value (cdr keys)))))
